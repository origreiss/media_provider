'use strict';

/**
 * @ngdoc directive
 * @name mediaProviderApp.directive:youtube
 * @description
 * # youtube
 */
angular.module('mediaProviderApp')
  .directive('media', function () {

    return {
      restrict: 'E',
      controller: function($scope) {
        $scope.link_results = {};

        // listen to results coming from media sources
        $scope.$on('found', function(event, result) {
          $scope.link_results[result[0]] = result[1];
        });

        // listen to results coming from media sources
        $scope.$on('embed', function(event, result) {
          $scope.link_results[result[0]] = result[1];
          $scope.embed_item(result[0], 0)
        });
      },
      scope: true,
      link: function(scope){
        function clearFields() {
          scope.link_value = '';
          scope.query_value = '';
        }

        scope.search_link = function(link) {
          scope.link_results = {};
          scope.$broadcast('link_search', link);
          clearFields()
        };

        scope.search_query = function(query) {
          scope.link_results = {};
          scope.$broadcast('query_search', query);
          clearFields()
        };

        scope.embed_item = function(source, key) {
          document.getElementById('embed_box').innerHTML = scope.link_results[source][key]['embed'];
          scope.link_results = {};
        };
      },
      templateUrl: 'views/media.html'
    };
  });
