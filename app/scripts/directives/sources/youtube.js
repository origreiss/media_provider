'use strict';

/**
 * @ngdoc directive
 * @name mediaProviderApp.directive:youtube
 * @description
 * # youtube
 */
angular.module('mediaProviderApp')
  .directive('youtube', ['youtube', function (youtube) {
    return {
      scope: true,
      require: '^media',
      link: function($scope){
        var preview = 'https://lh5.ggpht.com/jZ8XCjpCQWWZ5GLhbjRAufsw3JXePHUJVfEvMH3D055ghq0dyiSP3YxfSc_czPhtCLSO=w300',
            embed = undefined;

        // create an iframe tag for video embed with youtube video ID
        var get_embed_tag = function(id) {
          return '<iframe width="420" height="315" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allowfullscreen></iframe>';
        };

        // create youtube thumbnail src url with youtube video ID
        var get_preview_src = function(id) {
          return 'http://img.youtube.com/vi/'+id+'/0.jpg';
        };

        $scope.$on('link_search', function(event, link) {
          // parse link url and look for youtube ID
          var myregexp = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
          var res = link.match(myregexp);

          if (res) {
            // youtube ID detected - embed youtube video
            embed = get_embed_tag(res[1]);
            preview = get_preview_src(res[1]);

            $scope.$emit('embed', ['youtube', [{preview: preview, embed: embed}]]);
          }
        });

        $scope.$on('query_search', function(event, query) {
          // fetch data from youtube API
          youtube.async(query).then(function(d) {
            var results = d.items.map(function(obj) {
              return {preview: get_preview_src(obj.id.videoId), embed: get_embed_tag(obj.id.videoId)};
            });

            $scope.$emit('found', ['youtube', results]);
          });
        });
      },
      controller: function($scope) {

      }

    };
  }]);
