'use strict';

/**
 * @ngdoc directive
 * @name mediaProviderApp.directive:instagram
 * @description
 * # instagram
 */
angular.module('mediaProviderApp')
  .directive('instagram', function () {
    return {
      scope: true,
      require: '^media',
      link: function($scope,element,attr){
        var preview = 'https://lh3.googleusercontent.com/aYbdIM1abwyVSUZLDKoE0CDZGRhlkpsaPOg9tNnBktUQYsXflwknnOn2Ge1Yr7rImGk=w300',
            embed = '';

        $scope.$on('link_search', function(event, link) {

          // validate link
          if (false) {
            $scope.$emit('found', ['instagram', [{preview: preview, embed: embed}]]);
          }

        });
      }

    };
  });
