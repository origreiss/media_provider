'use strict';

/**
 * @ngdoc overview
 * @name mediaProviderApp
 * @description
 * # mediaProviderApp
 *
 * Main module of the application.
 */
angular
  .module('mediaProviderApp', [
    'ngAnimate'
  ]);
