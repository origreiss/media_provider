'use strict';

/**
 * @ngdoc function
 * @name mediaProviderApp.controller:MediaCtrl
 * @description
 * # MediaCtrl
 * Controller of the mediaProviderApp
 */
angular.module('mediaProviderApp')
  .controller('mediaCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.search_link = function(value) {
      console.log(value);
    };

    $scope.search_query = function(value) {
      console.log(value);
    };

  });
