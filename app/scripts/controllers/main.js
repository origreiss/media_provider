'use strict';

/**
 * @ngdoc function
 * @name mediaProviderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mediaProviderApp
 */
angular.module('mediaProviderApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


  });
