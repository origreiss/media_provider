'use strict';

/**
 * @ngdoc service
 * @name mediaProviderApp.youtube
 * @description
 * # youtube
 * Factory in the mediaProviderApp.
 */
angular.module('mediaProviderApp')
  .factory('youtube', function ($http) {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    var searchVideo = {
      async: function(query_string) {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get('https://www.googleapis.com/youtube/v3/search',{
          params: {
            key: 'AIzaSyCYZPjEZGTyNoN5KogxTOZj501xVItGUBk',
            type: 'video',
            maxResults : '5',
            part: 'id',
            // fields: 'items/id,items/snippet/title,items/snippet/description,items/snippet/thumbnails/default,items/snippet/channelTitle',
            q: query_string
          }
        }).then(function (response) {
          // The then function here is an opportunity to modify the response
          // The return value gets picked up by the then in the controller.
          return response.data;
        });
        // Return the promise to the controller
        return promise;
      }
    };
    return searchVideo;

  });
